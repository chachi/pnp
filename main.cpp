#include <Eigen/Eigen>
#include <Sophus/se3.hpp>
#include <iostream>
#include <limits>
#include <vector>

// To make the derivatives simpler for now, we use the entire 3x4 (in
// row-major order) matrix of the SE3 as the state vector rather than
// the tangent space.
using OptStateT = Eigen::Matrix<float, 12, 1>;

/** Store the 2D -> 3D correspondences. */
struct PointMatch {
  PointMatch(float x, float y, float z, float u, float v)
      : p_img(u, v), p_w(x, y, z) {
  }

  Eigen::Vector2f p_img; // Image coordinates
  Eigen::Vector3f p_w; // Global point coords
};

/** Storage structure for camera parameters. */
struct CamParams {
  CamParams(float _fx, float _fy, float _cx, float _cy, float _k1, float _k2)
      : fx(_fx), fy(_fy), cx(_cx), cy(_cy), k1(_k1), k2(_k2) {}

  float fx, fy, cx, cy;
  float k1, k2;
};

/** Compute distortion factor for a given radius */
float distortion_factor(float r2, const CamParams& params) {
  float r4 = r2 * r2;
  return (1 + r2 * params.k1 + r4 * params.k2);
}

/** Map a point on the z == 1 plane into the image, including distortion */
Eigen::Vector2f map(const CamParams& params,
                    const Eigen::Vector2f& p_cam_z1) {
  float r2 = p_cam_z1.squaredNorm();
  float r_factor = distortion_factor(r2, params);

  Eigen::Vector2f p_distorted = p_cam_z1 * r_factor;
  return Eigen::Vector2f(p_distorted[0] * params.fx + params.cx,
                         p_distorted[1] * params.fy + params.cy);
}

/** Compute the error for a single point */
Eigen::Vector2f error(const Sophus::SE3f& t_cw,
                      const PointMatch& match,
                      const CamParams& params) {
  Eigen::Vector3f p_cam = t_cw * match.p_w;
  Eigen::Vector2f p_cam_z1 = p_cam.head<2>() / p_cam[2];
  return match.p_img - map(params, p_cam_z1);
}

/** Compute residual vector (2 residuals per point) */
void residuals(const Sophus::SE3f& t_cw,
               const std::vector<PointMatch>& matches,
               const CamParams& params,
               Eigen::VectorXf* out) {
  out->resize(matches.size() * 2);
  for (size_t i = 0; i < matches.size(); ++i) {
    out->block<2, 1>(i * 2, 0) = error(t_cw, matches[i], params);
  }
}

/** Compute the residual jacobian wrt the input pose state. */
void jacobian(const Sophus::SE3f& t_cw,
              const std::vector<PointMatch>& matches,
              const CamParams& params,
              Eigen::MatrixXf* out) {
  // 2 residuals per point
  out->resize(matches.size() * 2, 12);

  // Derivative of point in camera wrt camera pose
  Eigen::Matrix<float, 3, 12> d_pcam_dtcw = Eigen::Matrix<float, 3, 12>::Zero();
  d_pcam_dtcw.block<3, 3>(0, 9) = Eigen::Matrix3f::Identity();

  // Derivative of point on z == 1 plane wrt point in camera space
  Eigen::Matrix<float, 2, 3> d_pz1_pcam;

  // Derivative of mapping function wrt point on z == 1
  Eigen::Matrix2f d_map_pz1;
  for (size_t i = 0; i < matches.size(); ++i) {
    const PointMatch& match = matches[i];
    Eigen::Vector3f p_cam = t_cw * match.p_w;
    float pz = p_cam[2];
    float pz2 = pz * pz;

    for (int j = 0; j < 3; ++j) {
      d_pcam_dtcw.block<1, 3>(j, j * 3) = match.p_w;
    }

    d_pz1_pcam <<
        1.0f / p_cam[2], 0.0f, -p_cam[0] / pz2,
        0.0f, 1.0f / p_cam[2], -p_cam[1] / pz2;

    Eigen::Vector2f p_cam_z1 = p_cam.head<2>() / pz;
    float r2 = p_cam_z1.squaredNorm();
    float r = sqrtf(r2);
    float distortion = distortion_factor(r2, params);

    float d_distortion_factor = 2 * r * params.k1 + 4 * r2 * r * params.k2;
    Eigen::Vector2f d_distort = d_distortion_factor * (1.0f / r) * p_cam_z1;

    d_map_pz1 <<
        d_distort[0] * params.fx * p_cam_z1[0] + distortion * params.fx,
        d_distort[1] * params.fx * p_cam_z1[0],

        d_distort[0] * params.fy * p_cam_z1[1],
        d_distort[1] * params.fy * p_cam_z1[1] + distortion * params.fy;

    Eigen::Matrix<float, 2, 12> dr_dtcw = -d_map_pz1 * d_pz1_pcam * d_pcam_dtcw;
    out->block<2, 12>(i * 2, 0) = dr_dtcw;
  }
}

/** Create SE3 object from state parameter vector */
Sophus::SE3f se3_from_x(const OptStateT& x) {
  Eigen::Vector3f translation = x.tail<3>();
  Eigen::Matrix3f rotation;
  rotation <<
      x[0], x[1], x[2],
      x[3], x[4], x[5],
      x[6], x[7], x[8];

  return Sophus::SE3f(rotation, translation);
}

int main(int argc, char* argv[]) {
  // Camera params
  static const float fx = 510,
      fy = 500, cx = 320, cy = 240, k1 = 0.02, k2 = -0.015;
  static const std::vector<PointMatch> matches = {
    {0.401191, -0.533441, 3.653708, 387.984711, 159.119141},
    {1.707242, -0.330801, 3.120764, 615.565918, 184.168625},
    {0.495830, 1.350876, 3.327281, 400.208130, 436.529327},
    {-0.480684, -0.240581, 1.522665, 170.405334, 144.448517},
    {-0.901928, 0.722427, 2.821982, 163.675308, 353.834656},
    {0.660500, 0.167810, 1.233900, 606.807495, 303.382812},
    {-0.279028, 0.177209, 2.684985, 275.692596, 262.025391},
    {0.497133, 0.471177, 2.243698, 440.613251, 338.473877},
    {-0.383256, -0.657891, 2.383663, 250.990723, 88.886246},
    {0.149257, -0.735220, 1.856616, 377.028290, 29.072174}
  };

  CamParams params(fx, fy, cx, cy, k1, k2);

  OptStateT x = OptStateT::Zero();

  // Rotation matrix starts as identity.
  x[0] = 1.0f;
  x[4] = 1.0f;
  x[8] = 1.0f;

  float step = std::numeric_limits<float>::max();
  const static float kSigma = 1e-3;

  Eigen::VectorXf res;
  Eigen::MatrixXf j;

  // Until stopping criteria
  while (step > kSigma) {
    Sophus::SE3f t_cw = se3_from_x(x);
    jacobian(t_cw, matches, params, &j);

    residuals(t_cw, matches, params, &res);
    OptStateT step_vec = (j.transpose() * j).inverse() * j.transpose() * res;
    x -= step_vec;
    step = step_vec.norm();

    std::cerr << "Jacobian: " << std::endl
              << j << std::endl;
    std::cerr << "Step: " << std::endl
              << step_vec.transpose() << std::endl;
    std::cerr << "Errors: " << res.norm() << std::endl;
    std::cerr << std::endl << "Pose: " << std::endl
              << t_cw.inverse().matrix()
              << std::endl;
  }
}
